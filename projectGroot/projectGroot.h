#ifndef PROJECT_GROOT_H
#define PROJECT_GROOT_H

/* ===== INCLUDES ===== */
#include <SevSeg.h>
#include <DHT.h>

/* ===== DEFINITIONS ===== */
// Type of DHT sensor used is 11
#define DHTTYPE DHT11
// Number of readings to average
#define BUFFER_SIZE 24
// Interval between readings in milliseconds
#define SLEEP_INTERVAL 5000
// Duration of average temperature display
#define TEMPERATURE_DISPLAY_DURATION 10000
// Number of digits on the display
#define NUMBER_OF_DIGITS 4
// Pin layout
#define DHTPIN 2
#define DIST_ECHO_PIN 10
#define DIST_TRIG_PIN 11
#define LOW_TEMP_LED_PIN 3
#define HIGH_TEMP_LED_PIN 4
#define RELAY_PIN 5
#define DIGIT_1_PIN 14
#define DIGIT_2_PIN 17
#define DIGIT_3_PIN 18
#define DIGIT_4_PIN 12
#define SEGMENT_A_PIN 15
#define SEGMENT_B_PIN 19
#define SEGMENT_C_PIN 7
#define SEGMENT_D_PIN 8
#define SEGMENT_E_PIN 9
#define SEGMENT_F_PIN 16
#define SEGMENT_G_PIN 6
#define DECIMAL_POINT_PIN 13
// Temperature thresholds
#define LOW_TEMP 20
#define HIGH_TEMP 24
#define HIGH_TEMP_RELAY 26
// Distance threshold
#define DIST_THRESHOLD 10
#define CASE_HIGH_TEMP 0
#define CASE_LOW_TEMP 1
#define CASE_AVERAGE_TEMP 2


/* ===== GLOBAL VARIABLES AND INITIALIZATIONS ===== */
// Initializes DHT sensor
DHT dht(DHTPIN, DHTTYPE);
// Instantiates a seven segment object
SevSeg sevseg;
int temperatureReadingsCounter;
float temperatures[BUFFER_SIZE];
float averageTemp = 0;
unsigned long lastReading = 0, displayTimeStart = 0;

/* ===== FUNCTION DEFINITIONS ===== */
void getNewSamples();
float getNewTemp();
float calcAverageTempAndReset();
float getDistance();

#endif //PROJECT_GROOT_H
