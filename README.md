# Microprocessors and Peripherals semester assignment, 2019, AUTH
> Temperature and distance sensing and data sending over the internet

Groot is an *experimental* application developed as part of the course "Microprocessors and Peripherals" assignment, that took place in the Department of Electrical & Computer Engineering at Aristotle University of Thessaloniki in 2019.

---

## Dependencies

In order to compile and execute this application the bellow referenced libraries and their dependencies are needed.

Libraries:
 - SevSeg
 - DHT
 - Adafruit_Sensor (DHT.h dependency)

All the libraries' latest versions (as of the completion of the project) can be found in the `dependencies` directory of this repository. Instructions for installing them can be found in the links bellow:
 - https://www.arduino.cc/en/Guide/Libraries#toc5
 - https://www.arduino.cc/en/hacking/libraries

---

## Execution

To execute the code just open the `projectGroot\projectGroot.ino` file using the Arduino IDE and click the Verify and Upload buttons. All necessery peripherials need to be correctly connected to the Arduino board used, more information about the connections can be found inside the `projectGroot.ino` file and the report.

---

## Support

Reach out to us:

- [apostolof's email](mailto:apotwohd@gmail.com "apotwohd@gmail.com")
- [anapt's email](mailto:ana.pachni.tsitiridou@gmail.com "ana.pachni.tsitiridou@gmail.com")

---
