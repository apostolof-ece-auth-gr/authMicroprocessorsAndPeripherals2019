/*
 * Simple code to test the temperature sensor function and electrical connections.
*/
#include <DHT.h>

// Type of DHT sensor used is 11
#define DHTTYPE DHT11
#define DHTPIN 2

// Initializes DHT sensor
DHT dht(DHTPIN, DHTTYPE);

/* ===== SETUP AND LOOP ===== */
void setup() {
  // Starts the serial com with PC
  Serial.begin(9600);
  Serial.println("Running setup function");

  dht.begin();
}

void loop() {
  // Reads temperature as Celsius
  float temperature = dht.readTemperature();

  // Checks if the read failed and handles the failure
  if (isnan(temperature)) {
    Serial.print(F("Failed to read from DHT sensor!"));
  }

  Serial.print(F("Temperature: "));
  Serial.print(temperature);
  Serial.println(F("°C "));

  // Waits a few seconds between measurements.
  delay(1000);
}
