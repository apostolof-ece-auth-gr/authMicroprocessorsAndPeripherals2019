/*
 * Simple code to test the display function and electrical connections.
*/
#include <SevSeg.h>

//Instantiates a seven segment object
SevSeg sevseg;

void setup() {
  byte numDigits = 4;
  byte digitPins[] = {14, 17, 18, 12};
  byte segmentPins[] = {15, 19, 7, 8, 9, 16, 6, 13};
  bool resistorsOnSegments = false;
  byte hardwareConfig = COMMON_CATHODE;
  bool updateWithDelays = false;
  bool leadingZeros = false;
  bool disableDecPoint = false;

  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments,
    updateWithDelays, leadingZeros, disableDecPoint);
}

void loop() {
  sevseg.setNumber(567, 1);
  sevseg.refreshDisplay();
  sevseg.setBrightness(120);
}
